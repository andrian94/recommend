var app = app || {};
app.UsersView = Backbone.View.extend({
    el: '#users',
    events: {},
    initialize: function() {
        this.collection = new app.Users();
        this.collection.fetch();
        this.render();
        //console.log(this.collection);
    },
    render: function() {
        console.log(this.collection);
        this.collection.forEach(function (item) {
            console.log(item);
            this.renderUser(item);
        }, this);
    },
    renderUser: function(item) {
        console.log("RenderUser Collection");
        var userView = new app.UserView({
          model: item
        })
        this.$el.append(userView.render().el);
    }
});
