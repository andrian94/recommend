var app = app || {};
app.FilmView = Backbone.View.extend({
	tagName: 'div',
	className: 'filmContainer',
	events: {
		'click .details': 'details',
		'click .star': 'rate'
	},
	initialize : function() {
		this.template= _.template( $('#filmTemplate').html() );
	},
	render: function() {
		this.$el.html( this.template( this.model.toJSON() ));
		//Plot stars
        // var rate = this.model.get('userRate');
        // if (rate == null || rate == 'undefined' || rate == 0)
        //     this.$el.find("#filmTemplatePlotStars").html(plotStars('null'));
        // else
        //     this.$el.find("#filmTemplatePlotStars").html(plotStars(rate));
		$.ajax({
			type: "POST",
			url: "/film/getRate",
			data: {
				filmId: this.model.get('id')
			},
			success: function(data){
				if(data!=null){
					this.$el.find("#filmTemplatePlotStars").html(plotStars(data.userRate));
				}else{
					this.$el.find("#filmTemplatePlotStars").html(plotStars('null'));
				}
			}.bind(this)
		})
		//Plot stars End
		return this;
	},
	details: function() {
		var filmID = this.model.get('id');
        var comments = [{
                "name":"Helga",
                "film_id":"tt10101010",
                "comment":"This is the best movie I’ve ever seen. The actors are amazing, and I just love young Leo and his smile.",
                "class":"good"
            },{
                "name":"John",
                "film_id":"tt10101010",
                "comment":"I thought Terminator 3 was a bad movie. But this one is ridiculous… The jokes are dumb, and the CGI looks like it was rendered with the toaster.",
                "class":"bad"
            },{
                "name":"Martin",
                "film_id":"tt10101010",
                "comment":"So wow. Much fabulous. Doge is satisfied. Hatiko forever.",
                "class":"good"
            }];
		$.ajax({
			type: "POST",
			url: "/film/watch",
			data: {
				film: filmID
			},
			success: function(data){
				console.log("details");
				console.log(data);
			}
		});
		$('#posterTempl').attr("src", this.model.get('poster'));
		$('#titleTempl').html(this.model.get('title')+' <small>('+this.model.get('year')+')</small>');
		$('#titleTempl').attr('filmID', this.model.get('id'));
		$('#genreTempl').text(this.model.get('genre'));
		$('#countryTempl').text(this.model.get('country'));
		$('#languageTempl').text(this.model.get('language'));
		$('#releasedTempl').text(this.model.get('released'));
		$('#typeTempl').text(this.model.get('type'));
		$('#runtimeTempl').text(this.model.get('runtime'));
		$('#ratedTempl').text(this.model.get('rated'));
		$('#actorsTempl').text(this.model.get('actors'));
		$('#awardsTempl').text(this.model.get('awards'));
		$('#imdbRatingTempl').text(this.model.get('imdbRating'));
		$('#imdbVotesTempl').text(this.model.get('imdbVotes'));
		$('#plotTempl').html(this.model.get('plot'));
		document.body.style.overflow = 'hidden';
		$('#modalDetails').modal('show');
		var stars = this.$el.find("#filmTemplatePlotStars").html();
		$("#plotStars").html(stars);
        comments.forEach(function(c) {
            $('#comments').append('<div class="jumbotron aaaaa '+ c.class +'"><h4>'+ c.name +'</h4><div>'+ c.comment +'</div></div>');
        });
	},
	rate: function(e){
		e.preventDefault();
		id = this.model.get('id');
		this.$el.find(".star").attr('data-filmid', id);
		var rate = $(e.currentTarget).data("id");
		var filmID = $(e.currentTarget).data("filmid");
		if (filmID == undefined){
			filmID = this.model.get('id');
		}
		console.log('FilmID - ' + filmID + " Rate - " + rate);
		$.ajax({
			type: "POST",
			url: "/film/rate",
			data: {
				filmId: filmID,
				rate: rate
			},
			success: function(data){
				$.ajax({
					type: "POST",
					url: "/film/getRate",
					data: {
						filmId: this.model.get('id')
					},
					success: function(data){
						if(data!=null){
							console.log('not NULL');
							this.$el.find("#filmTemplatePlotStars").replaceWith('<ul id="filmTemplatePlotStars">' + plotStars(data.userRate) + '</ul>');
						}else{
							console.log('NULL')
							this.$el.find("#filmTemplatePlotStars").replaceWith('<ul id="filmTemplatePlotStars">' + plotStars('null') + '</ul>');
						}
					}.bind(this)
				});
			}.bind(this)
		});
	}
});
