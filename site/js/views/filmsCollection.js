var app = app || {};
app.FilmsView = Backbone.View.extend({
  el: '#films',
  events: {},
  initialize: function() {
    this.collection = new app.Films();
    this.collection.fetch();
    this.render();
    this.listenTo(this.collection, 'add', this.renderFilm);
    this.listenTo(this.collection, 'reset', this.render);
    this.listenTo(Backbone.Events, 'findFilm', this.findFilm, this);
    // Sort collection by time and reRendering
    this.listenTo(Backbone.Events, 'byTime', this.byTime, this);
    this.listenTo(Backbone.Events, 'byTime', this.render, this);
    // Sort collection by Rate and reRendering
    this.listenTo(Backbone.Events, 'byRate', this.byRate, this);
    this.listenTo(Backbone.Events, 'byRate', this.render, this);
    // Sort collection by Year and reRendering
    this.listenTo(Backbone.Events, 'byYear', this.byYear, this);
    this.listenTo(Backbone.Events, 'byYear', this.render, this);
    // Recommendations and reRendering
    this.listenTo(Backbone.Events, 'recommend', this.recommend, this);
    this.listenTo(Backbone.Events, 'recommend', this.render, this);
    // All films
    this.listenTo(Backbone.Events, 'allFilms', this.allFilms, this);
    this.listenTo(Backbone.Events, 'allFilms', this.render, this);
    // All films Admin Profile
    this.listenTo(Backbone.Events, 'allFilmsAdmin', this.allFilmsAdmin, this);
    // Render stars
    this.listenTo(Backbone.Events, 'renderStars', this.renderStars, this);
    // Aubmit ASP
    this.listenTo(Backbone.Events, 'submitASP', this.submitASP, this);
  },
  render: function() {
    this.collection.each(function(item) {
      this.renderFilm(item);
    }, this);
  },
  renderFilm: function(item) {
    var filmView = new app.FilmView({
        model: item
    });
    this.$el.append(filmView.render().el);
  },
  findFilm: function() {
    console.log('findFilm');
    var name = $('#findFilm').val();
    this.collection.fetch({
        type: "POST",
        url: "/film/find",
        data: {
            name: name
        },
        success: function(data) {
          this.render();
        }.bind(this)
    });
  },
  byTime: function() {
    console.log('Sort byTime');
    var i, j;
    var tmp = new Object();
    if (!$('.normalByTime').hasClass("active") && !$('.revByTime').hasClass("active")) {
      $('.normalByTime').button('toggle');
    }
    if ($('.normalByTime').hasClass("active")) {
      for (i = 0; i < this.collection.length; i++) {
        for (j = 0; j < this.collection.length - 1; j++) {
          if (!getRuntime(this.collection.models[j].get('runtime'), this.collection.models[j + 1].get('runtime'))) {
            tmp = this.collection.models[j];
            this.collection.models[j] = this.collection.models[j + 1];
            this.collection.models[j + 1] = tmp;
          }
        }
      }
    } else {
      for (i = 0; i < this.collection.length; i++) {
        for (j = 0; j < this.collection.length - 1; j++) {
          if (getRuntime(this.collection.models[j].get('runtime'), this.collection.models[j + 1].get('runtime'))) {
            tmp = this.collection.models[j];
            this.collection.models[j] = this.collection.models[j + 1];
            this.collection.models[j + 1] = tmp;
          }
        }
      }
    }

  },
  byRate: function() {
    console.log('Sort byRate');
    var i, j;
    var tmp = new Object();
    if (!$('.normalbyIMDB').hasClass("active") && !$('.revbyIMDB').hasClass("active")) {
      $('.normalbyIMDB').button('toggle');
    }
    if ($('.normalbyIMDB').hasClass("active")) {
      for (i = 0; i < this.collection.length; i++) {
        for (j = 0; j < this.collection.length - 1; j++) {
          if (this.collection.models[j].get('imdbRating') < this.collection.models[j + 1].get('imdbRating')) {
            tmp = this.collection.models[j];
            this.collection.models[j] = this.collection.models[j + 1];
            this.collection.models[j + 1] = tmp;
          }
        }
      }
    } else {
      for (i = 0; i < this.collection.length; i++) {
        for (j = 0; j < this.collection.length - 1; j++) {
          if (this.collection.models[j].get('imdbRating') > this.collection.models[j + 1].get('imdbRating')) {
            tmp = this.collection.models[j];
            this.collection.models[j] = this.collection.models[j + 1];
            this.collection.models[j + 1] = tmp;
          }
        }
      }
    }

  },
  byYear: function() {
    var i, j;
    var tmp = new Object();
    if (!$('.normalbyYear').hasClass("active") && !$('.revbyYear').hasClass("active")) {
      $('.normalbyYear').button('toggle');
    }
    if ($('.normalbyYear').hasClass("active")) {
      for (i = 0; i < this.collection.length; i++) {
        for (j = 0; j < this.collection.length - 1; j++) {
          if (this.collection.models[j].get('year') < this.collection.models[j + 1].get('year')) {
            tmp = this.collection.models[j];
            this.collection.models[j] = this.collection.models[j + 1];
            this.collection.models[j + 1] = tmp;
          }
        }
      }
    } else {
      for (i = 0; i < this.collection.length; i++) {
        for (j = 0; j < this.collection.length - 1; j++) {
          if (this.collection.models[j].get('year') > this.collection.models[j + 1].get('year')) {
            tmp = this.collection.models[j];
            this.collection.models[j] = this.collection.models[j + 1];
            this.collection.models[j + 1] = tmp;
          }
        }
      }
    }
  },
  recommend: function() {
    this.collection.reset();
    this.collection.url = "/film/recommendations";
    this.collection.fetch({
        success: function (){
            if (this.collection.models[0].get("title") == 'No name') {
                $(".filmContainer").hide();
                $(".user-info-no-reccomended").show();
            }
        }.bind(this)
    });
    console.log("fetch() by url:" + this.collection.url);
  },
  allFilms: function() {
    this.collection.reset();
    this.collection.url = "/film/all";
    this.collection.fetch();
    console.log("fetch() by url:" + this.collection.url);
  },
  allFilmsAdmin: function(){
      $('.usersContainer').hide();
      $("div.filmContainer tr").unwrap();
      $(".filmsContainer").show();
  },
  renderStars: function() {
    $('#films').empty();
    this.render();
  },
  submitASP: function(){
    var data = new Object();
    data.minImdb = $('#slider1').slider('values', 0 );
    data.maxImdb = $('#slider1').slider('values', 1 );
    data.minYear = $('#slider2').slider('values', 0 );
    data.maxYear = $('#slider2').slider('values', 1 );
    data.Type = $("input:checked").val();
    $('#films').empty();
    this.collection.reset();
    this.collection.url = "/film/criteria";
    this.collection.fetch({
        type: "POST",
        data: {
            minImdb: data.minImdb,
            maxImdb: data.maxImdb,
            minYear: data.minYear,
            maxYear: data.maxYear,
            type: data.Type
        }
    });
  }
});
