var app = app || {};
app.UserView = Backbone.View.extend({
	tagName: 'tr',
	className: 'userContainer',
	events: {
		'click .deleteUser': 'deleteUser',
	},
	initialize : function() {
		this.template= _.template( $('#userTemplate').html() );
	},
	render: function() {
		this.$el.html( this.template( this.model.toJSON() ));
		return this;
	},
	deleteUser: function(){
		console.log(this.model);
	}
});
