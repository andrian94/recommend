var app = app || {};
app.UsersView = Backbone.View.extend({
    el: '#users',
    events: {},
    initialize: function() {
        this.collection = new app.Users();
        // All users Admin Profile
        this.listenTo(Backbone.Events, 'allUsersAdmin', this.allUsersAdmin, this);
        // Recommended users
        this.listenTo(Backbone.Events, 'recommendUsers', this.recommendUsers, this);
        // Most Active users
        this.listenTo(Backbone.Events, 'mostActive', this.mostActive, this);
    },
    render: function() {
        this.collection.each(function(item){
          this.renderUser(item);
        }, this);
    },
    renderUser: function(item) {
        var userView = new app.UserView({
          model: item
        })
        this.$el.append(userView.render().el);
    },
    allUsersAdmin: function () {;
        $(".filmsContainer").hide()
        this.collection.reset();
        this.collection.url = "/user/allUsers";
        this.collection.fetch({
            success: function(){
                this.render();
                $('.hideAddFilms').hide();
                $(".usersContainer").show();
            }.bind(this)
        });
    },
    recommendUsers: function () {
        this.collection.reset();
        this.collection.url = "/user/recommendation";
        this.collection.fetch({
            success: function(){
                this.render();
                $(".usersContainer").show();
            }.bind(this)
        });
    },
    mostActive: function () {
        this.collection.reset();
        this.collection.url = "/user/mostActiveAdding";
        this.collection.fetch({
            type: "POST",
            data: {
                number: $(".mostMostMostActiveUsers").val()
            },
            success: function(){
                this.render();
                $(".usersContainer").show();
                $('.hideAddFilms').show();
            }.bind(this)
        });
    }
});
