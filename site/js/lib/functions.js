var app = app || {};
$( document ).ready(function() {
	//=== Collection Events Triggers ===\\\

    $('#findFilm').keyup(function(e){
	    if(e.keyCode == 13){
	    	$('#films').empty();
	    	_.extend(app.FilmsView, Backbone.Events);
			app.FilmsView.trigger("findFilm");
	    }
	});

	$('.byTime1').click(function(){
    	$('#films').empty();
    	_.extend(app.FilmsView, Backbone.Events);
		app.FilmsView.trigger("byTime");
	});
    $('.byTime2').click(function(){
    	$('#films').empty();
    	_.extend(app.FilmsView, Backbone.Events);
		app.FilmsView.trigger("byTime");
	});

	$('.byRate1').click(function(){
    	$('#films').empty();
    	_.extend(app.FilmsView, Backbone.Events);
		app.FilmsView.trigger("byRate");
	});

    $('.byRate2').click(function(){
    	$('#films').empty();
    	_.extend(app.FilmsView, Backbone.Events);
		app.FilmsView.trigger("byRate");
	});

	$('.byYear1').click(function(){
    	$('#films').empty();
    	_.extend(app.FilmsView, Backbone.Events);
		app.FilmsView.trigger("byYear");
	});

    $('.byYear2').click(function(){
    	$('#films').empty();
    	_.extend(app.FilmsView, Backbone.Events);
		app.FilmsView.trigger("byYear");
	});

	$('#recommend').click(function(){
    	$('#films').empty();
        $('#users').empty();
        $(".ratesHide").show();
    	_.extend(app.FilmsView, Backbone.Events);
		app.FilmsView.trigger("recommend");
	});

	$('#allFilms').click(function(){
    	$('#films').empty();
        $('#users').empty();
        $(".ratesHide").show();
        $(".user-info-no-reccomended").hide();
    	_.extend(app.FilmsView, Backbone.Events);
		app.FilmsView.trigger("allFilms");
	});

	$('#closeModalDetails').click(function(){
    	_.extend(app.FilmsView, Backbone.Events);
		app.FilmsView.trigger("renderStars");
	});

    $('#allFilmsAdmin').click(function(){
        $('.statistic').hide();
    	_.extend(app.FilmsView, Backbone.Events);
		app.FilmsView.trigger("allFilmsAdmin");
	});
    $('#getAllUsers').click(function(){
        $('.statistic').hide();
        $('#users').empty();
    	_.extend(app.UsersView, Backbone.Events);
		app.UsersView.trigger("allUsersAdmin");
	});
    $('.sendOkMostActive').click(function(){
        $('#users').empty();
    	_.extend(app.UsersView, Backbone.Events);
		app.UsersView.trigger("mostActive");
	});
    $('#getStatistic').click(function(){
        $('.usersContainer').hide();
        $('.filmsContainer').hide();
        $('.statistic').show();
	});
    $('#recommendUsers').click(function(){
        $(".filmContainer").hide();
        $(".ratesHide").hide();
        $(".user-info-no-reccomended").hide();
        $("#users").empty();
    	_.extend(app.UsersView, Backbone.Events);
		app.UsersView.trigger("recommendUsers");
	});
    $('#submitASP').click(function(){
    	_.extend(app.FilmsView, Backbone.Events);
		app.FilmsView.trigger("submitASP");
	});

	//=== AJAX request - Add film by IMDB ID (No needs for collection)===\\

	$('#addFilm').keyup(function(e){
	    if(e.keyCode == 13){
	    	console.log('addFilm');
	    	var id = $('#addFilm').val();
	    	console.log(id);
			$.ajax({
				type: "POST",
				url: "/film/add",
				data: {
					id: id
				},
				success: function(data){
					$('#hideAlert').fadeIn();
					$('#addFilm').val('');
                    $('#addMSG').text(data);
				}
			});
	    }
	});

    //=== AJAX Delete Film ===\\

    $('body').on('click', '.deleteFilm', function() {
        var filmid = $(this).attr("data-id");
        $.ajax({
            type: "POST",
            url: "/film/delete",
            data: {
                id: filmid
            },
            success: function(data){
                console.log("Delete" - data);
                $(this).parent().parent().fadeOut();
            }.bind(this)
        });
	});

	//=== Sort type===\\

		//=== Sort by Time===\\
		$('.normalByTime').click(function(){
			if( $('.revByTime').hasClass("active") ){
				$('.revByTime').button('toggle');
			}
			$('.normalByTime').blur();
		});

		$('.revByTime').click(function(){
			if( $('.normalByTime').hasClass("active") ){
				$('.normalByTime').button('toggle');
			}
			console.log('revByTime');
			$('.revByTime').blur();
		});
		//=== Sort by imdb===\\
		$('.normalbyIMDB').click(function(){
			if( $('.revbyIMDB').hasClass("active") ){
				$('.revbyIMDB').button('toggle');
			}
			$('.normalbyIMDB').blur();
		});

		$('.revbyIMDB').click(function(){
			if( $('.normalbyIMDB').hasClass("active") ){
				$('.normalbyIMDB').button('toggle');
			}
			console.log('revbyIMDB');
			$('.revbyIMDB').blur();
		});
		//=== Sort by year===\\
		$('.normalbyYear').click(function(){
			if( $('.revbyYear').hasClass("active") ){
				$('.revbyYear').button('toggle');
			}
			$('.normalbyYear').blur();
		});

		$('.revbyYear').click(function(){
			if( $('.normalbyYear').hasClass("active") ){
				$('.normalbyYear').button('toggle');
			}
			console.log('revbyYear');
			$('.revbyYear').blur();
		});

	// Rate film from Details
	$('body').on('click', 'ul#plotStars > li.star', function() {
		var filmID = $("#titleTempl").attr('filmid');
		var rate = $(this).attr('data-id');
	    $.ajax({
			type: "POST",
			url: "/film/rate",
			data: {
				filmId: filmID,
				rate: rate
			},
			success: function(data){
				$.ajax({
					type: "POST",
					url: "/film/getRate",
					data: {
						filmId: filmID
					},
					success: function(data){
						if(data!=null){
							console.log('not NULL');
							$("#plotStars").replaceWith('<ul id="plotStars">' + plotStars(data.userRate) + '</ul>');
						}else{
							console.log('NULL')
							$("#plotStars").replaceWith('<ul id="plotStars">' + plotStars('null') + '</ul>');
						}
					}
				});
			}
		});
	});
});

//=== Helpful functions ===\\

// Parse and Compare Runtimes
function getRuntime (array1, array2){
	var time1, time2,i;
	time1 = '';
	time2 = '';
	i=0;
	while (array1[i] != ' '){
		time1 += array1[i];
		i++;
	}
	i=0;
	while (array2[i] != ' '){
		time2 += array2[i];
		i++;
	}
	if (parseInt(time1)>parseInt(time2)) {
		return true;
	}else{
		return false;
	}
}

// Plot Stars
function plotStars(data){
	switch(data) {
		case 'null':
			stars = '<li class="star" data-id="10"></li><li class="star" data-id="9"></li><li class="star" data-id="8"></li><li class="star" data-id="7"></li><li class="star" data-id="6"></li><li class="star" data-id="5"></li><li class="star" data-id="4"></li><li class="star" data-id="3"></li><li class="star" data-id="2"></li><li class="star" data-id="1"></li>';
			return stars;
		case 1:
			stars = '<li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="filledStar"></li>';
			return stars;
		case 2:
			stars = '<li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="filledStar"></li><li class="filledStar"></li>';
			return stars;
		case 3:
			stars = '<li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li>';
			return stars;
		case 4:
			stars = '<li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li>';
			return stars;
		case 5:
			stars = '<li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li>';
			return stars;
		case 6:
			stars = '<li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li>';
			return stars;
		case 7:
			stars = '<li class="emptyStar"></li><li class="emptyStar"></li><li class="emptyStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li>';
			return stars;
		case 8:
			stars = '<li class="emptyStar"></li><li class="emptyStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li>';
			return stars;
		case 9:
			stars = '<li class="emptyStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li>';
			return stars;
		case 10:
			stars = '<li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li><li class="filledStar"></li>';
			return stars;
	}
}

// Scrolling On
$(document).on('hide.bs.modal','#modalDetails', function () {
	document.body.style.overflow = 'visible';
});
