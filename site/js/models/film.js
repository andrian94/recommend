var app = app || {};
app.Film = Backbone.Model.extend({
    defaults: {
        title: 'No name',
        year: 0,
        rated: "Unknown",
        released: 'Unknown',
        runtime: 'Unknown',
        genre: 'Unknown',
        director: 'Unknown',
        writer:'Unknown',
        actors: 'Unknown',
        plot: 'Unknown',
        language: 'Unknown',
        country: 'Unknown',
        awards: 'Unknown',
        poster: 'http://placehold.it/320x150',
        imdbRating: 0,
        imdbVotes: 0,
        imdbID: 'Unknown',
        type: 'Unknown',
        userRate: 0,
        watched: 0,
        score: undefined
    },
    parse: function( response ) {
    	response.id = response._id;
    	return response;
    }
});
