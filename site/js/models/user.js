var app = app || {};
app.User = Backbone.Model.extend({
    defaults: {
        facebook_name: undefined,
        google_name: undefined,
        google_photo: undefined,
        google_url: undefined,
        vk_name: undefined,
        vk_photo: undefined,
        vk_url: undefined,
        local_name: undefined,
        telegram: undefined,
        id: undefined,
        filmAdded: undefined
    }
});
