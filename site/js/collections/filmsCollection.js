var app = app || {};
app.Films = Backbone.Collection.extend({
	model: app.Film,
	url: '/film/all'
});