﻿// get all tools we need
var express = require('express');
var app = express();
var port = process.env.PORT || 8080;

var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');
var fs = require('fs');
var FileStreamRotator = require('file-stream-rotator');

var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');

var MongoStore = require('connect-mongo')(session);

var configDB = require('./config/database.js');

var film = require('./controllers/filmController');
var userController = require('./controllers/userController.js');
var recommendControl = require('./controllers/recommendController.js');
var stat = require('./controllers/statController.js');
//var Bot = require('./config/bot.js');
var logDirectory = __dirname + '/log';

// ensure log directory exists
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

// create a rotating write stream
/*var accessLogStream = FileStreamRotator.getStream({
 filename: logDirectory + '/access-%DATE%.log',
 frequency: 'daily',
 verbose: true
 });*/

app.use(express.static('site'));
mongoose.connect(configDB.url);
require('./config/passport')(passport);

// app.use(morgan('dev', { stream: accessLogStream })); // log every request to the console
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.set('view engine', 'ejs');

app.use(session({
    secret: 'whatiamdoing',
    store: new MongoStore({
        mongooseConnection: mongoose.connection
    }),
    //cookie: {secure: true}
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

require('./app/routes.js')(app, passport, film, userController, recommendControl, stat);

app.listen(port);
console.log('The server starts on port ' + port);
