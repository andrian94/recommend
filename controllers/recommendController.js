﻿var Rate = require('../app/models/rate.js');
var Film = require('../app/models/film.js');
var User = require('../app/models/user');
var PreferenceUser = require("../app/models/prefByUser");

var NeuralNetworkController = require('./NeuralNetworkController');

var pearson = function (prefs, person1, person2) {
    var s1 = 0, s2 = 0, s1s = 0, s2s = 0, msum = 0, sum = 0;
    var c = 0;
    for (var item in prefs[person1]) {
        if (item in prefs[person2]) {
            c += 1;
            //sum += Math.pow(prefs[person1][item] - prefs[person2][item], 2);
            s1 += prefs[person1][item];
            s2 += prefs[person2][item];
            s1s += Math.pow(prefs[person1][item], 2);
            s2s += Math.pow(prefs[person2][item], 2);
            msum += prefs[person1][item] * prefs[person2][item];
        }
    }
    var num = msum - (s1 * s2 / c);
    var avg1 = Math.pow(s1, 2) / c;
    var avg2 = Math.pow(s2, 2) / c;
    var den = Math.sqrt((s1s - avg1 === 0 ? s1s + .1 - avg1 : s1s - avg1) * (s2s - avg2 === 0 ? s2s + .1 - avg2 : s2s - avg2));
    if (c === 0) return 0;
    //if (den === 0 || num === 0) return 1 / (1 + sum);
    var r = num / den;
    return r;
}

var topMatches = function (prefs, person, n, similarity) {
    var scores = [];
    //console.log(cluster.worker.id);
    for (var other in prefs) { //different workers have different 'other' variables
        if (other != person) {
            scores.push({'name': other, 'score': similarity(prefs, person, other)});
        }
    }
    //reduce all scores
    scores.sort(function (a, b) {
        if (a.score > b.score) return 1;
        if (a.score < b.score) return -1;
        else return 0;
    });
    scores.reverse();
    return scores.slice(0, n);
}

var getRecomendation = function (prefs, person, similarity) {
    var totals = {};
    var simSum = {};
    for (var other in prefs) {
        if (other != person) {
            var sim = similarity(prefs, person, other);
            //console.log(sim);
            if (sim > 0) {
                for (var item in prefs[other]) {
                    if (!(item in prefs[person])) {
                        totals[item] = 0;
                        totals[item] += prefs[other][item] * sim;
                        simSum[item] = 0;
                        simSum[item] += sim;
                    }
                }
            }
        }
    }
    var rankings = [];
    for (var i in totals) {
        rankings.push({'movie': i, 'score': totals[i] / simSum[i]});
    }
    rankings.sort(function (a, b) {
        if (a.score > b.score) return 1;
        if (a.score < b.score) return -1;
        else return 0;
    });
    return rankings.reverse();
}

var transform = function (prefs) {
    result = {};
    for (var person in prefs) {
        for (var item in prefs[person]) {
            if (!(item in result))
                result[item] = {};
            result[item][person] = prefs[person][item];
        }
    }
    return result;
}

var calculateSimilarItems = function (prefs, n) {
    var result = {};
    var itemPrefs = transform(prefs);
    for (var item in itemPrefs) {
        var scores = topMatches(itemPrefs, item, n, pearson);
        result[item] = scores;
    }
    return result;
};

var calculateSimilarPersons = function (prefs, n) {
    var result = {};
    for (var item in prefs) {       //cluster it
        var scores = topMatches(prefs, item, n, pearson);
        result[item] = scores;
    } //reduce data
    return result;
};

var getRecommendedItems = function (prefs, itemMatch, user) {
    var userRatings = prefs[user];
    var scores = {},
        totalSim = {};

    for (var item in userRatings) { //item - 'movie'
        for (var obj in itemMatch[item]) {  //obj - {name:,score:}
            if (!(itemMatch[item][obj].name in userRatings)) {
                if (!(itemMatch[item][obj].name in scores)) {
                    scores[itemMatch[item][obj].name] = 0;
                }
                scores[itemMatch[item][obj].name] += userRatings[item] * itemMatch[item][obj].score;
                if (!(itemMatch[item][obj].name in totalSim)) {
                    totalSim[itemMatch[item][obj].name] = 0;
                }
                totalSim[itemMatch[item][obj].name] += itemMatch[item][obj].score;
            }
        }
    }
    var rankings = [];
    for (var item in scores) {
        rankings.push({'score': totalSim[item] === 0 ? 0 : ((scores[item]) / totalSim[item]), 'name': item});
    }
    rankings.sort(function (a, b) {
        if (a.score > b.score) return 1;
        if (a.score < b.score) return -1;
        else return 0;
    });
    return rankings.reverse();
}

var saveSimilarUsers = function () {
    PreferenceUser.findOne({}, function (err, res) {
        var pref = res.users;
        var sim = calculateSimilarPersons(pref, Object.keys(pref).length);
        PreferenceUser.findOneAndUpdate({}, {similarUsers: sim}, {upsert: true}, function (err, res) {
            return;
        });
    });
}

var saveSimilarItems = function () {
    PreferenceUser.findOne({}, function (err, res) {
        var pref = res.users;
        var sim = calculateSimilarItems(pref, Object.keys(pref).length);
        PreferenceUser.findOneAndUpdate({}, {similarItems: sim}, {upsert: true}, function (err, res) {
            //saveSimilarUsers();
            return;
        });
    });
}

var usersTransformer = function (callback) {
    var pr = {};
    Rate.find({}, function (err, res) {
        for (element of res) {
            var user = element.userID;
            var film = element.filmID;
            if (pr[user] === undefined) pr[user] = {};
            pr[user][film] = element.userRate;
        }
        /* var bulk = PreferenceUser.collection.initializeOrderedBulkOp();
         var counter = 0;
         for (var user in pr) {
         bulk.find({user: {$exist: true}}).upsert().updateOne({user: pr[user]});
         counter++;
         if (counter % 100 === 0) {
         bulk.execute((err, res) => {
         bulk = PreferenceUser.collection.initializeOrderedBulkOp()
         });
         }
         }
         if (counter % 1000 != 0) {
         bulk.execute(function (err, result) {
         //saveSimilarItems();
         });
         }*/
        PreferenceUser.findOneAndUpdate({}, {users: pr}, {upsert: true}, function (err, res) {
            return callback();
        });
    });
};

var getSimilarUsers = function (user, callback) {
    //usersTransformer();
    PreferenceUser.findOne({}, function (err, res) {
        var users = res.similarUsers[user];
        //SimilarUsers.findOne({user: {$exist: true}}, users, (err, users) => {
        if (users == 'undefined' || users == null || users.length == 0) {
            return callback({});
        }
        var ids = [];
        for (var i in users) {
            ids.push(users[i].name);
        }

        User.find({_id: {$in: ids}}, function (err, result) {
            if (err || result == "undefined" || result.length == 0) {
                return callback({});
            }
            for (var i = 0; i < result.length; i++) {
                users[i].facebook_name = result[i].facebook.name;
                users[i].facebook_url = result[i].facebook.email;
                users[i].vk_name = result[i].vk.name;
                users[i].vk_url = result[i].vk.email;
                users[i].vk_photo = result[i].vk.photo;
                users[i].google_name = result[i].google.name;
                users[i].google_url = result[i].google.email;
                users[i].google_photo = result[i].google.photo;
                users[i].local_name = result[i].local.email;
                users[i].id = result[i]._id;
                users[i].telegram = result[i].telegram;
                users[i].isAdmin = result[i].admin;
            }
            return callback(users);
        });
    });
}

var getRecommendedFilms = function (user, callback) {
    //usersTransformer();
    PreferenceUser.findOne({}, function (err, p) {
        var pref = p.users;
        //SimilarItems.findOne({user: {$exist: true}}, (err, pref) => {
        var recommend = getRecommendedItems(pref, p.similarItems, user);
        if (recommend.length == 0) {
            return callback({});
        }

        NeuralNetworkController.getNeuralNetworkRecommend(user, (neuralNetworkRecommend) => {
            neuralNetworkRecommend = neuralNetworkRecommend.filter((filmNN) => {
                return recommend.find((filmP) => {
                        return filmP.name.toString() == filmNN.name.toString();
                    }) == undefined;
            });
            recommend = recommend.concat(neuralNetworkRecommend);
            if (neuralNetworkRecommend != []) recommend.sort((a, b) => {
                if (a.score > b.score) return 1;
                if (a.score < b.score) return -1;
                else return 0;
            }).reverse();

            var ids = recommend.map((film) => {
                return film.name;
            });

            Film.find({_id: {$in: ids}}, function (err, res) {
                if (err || res == 'undefined') {
                    return callback({});
                }
                recommend = recommend.map((recommend) => {
                    var filmInfo = res.find((film) => {
                        return film._id.toString() == recommend.name.toString();
                    });
                    /*recommend._id = recommend[j].name;
                     delete recommend[j].name;
                     recommend[j].title = res[j].title;
                     recommend[j].year = res[j].year;
                     recommend[j].rated = res[j].rated;
                     recommend[j].released = res[j].released;
                     recommend[j].runtime = res[j].runtime;
                     recommend[j].genre = res[j].genre;
                     recommend[j].director = res[j].director;
                     recommend[j].writer = res[j].writer;
                     recommend[j].actors = res[j].actors;
                     recommend[j].plot = res[j].plot;
                     recommend[j].language = res[j].language;
                     recommend[j].country = res[j].country;
                     recommend[j].awards = res[j].awards;
                     recommend[j].poster = res[j].poster;
                     recommend[j].imdbRating = res[j].imdbRating;
                     recommend[j].imdbVotes = res[j].imdbVotes;
                     recommend[j].imdbID = res[j].imdbID;
                     recommend[j].type = res[j].type*/
                    var obj = Object.assign({}, recommend, filmInfo.toObject());
                    delete obj.name;
                    return obj;
                });
                return callback(recommend);
            });

        });

    });
}

module.exports.getRecommendedFilms = getRecommendedFilms;
module.exports.usersTransformer = usersTransformer;
module.exports.getSimilarUsers = getSimilarUsers;
module.exports.saveSimilarUsers = saveSimilarUsers;
module.exports.saveSimilarItems = saveSimilarItems;
module.exports.topMatches = topMatches;
module.exports.usersTransformer = usersTransformer;
