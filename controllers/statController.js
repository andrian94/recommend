﻿var RateMap = require('../app/models/ratemap.js');
var User = require('../app/models/user.js');
var Rates = require('../app/models/rate.js');
var Film = require('../app/models/film.js');

var incrementRatesStat = function (currentRate){
    RateMap.findOneAndUpdate({ rate: currentRate }, { $inc : { count: 1 } }, { upsert : true }, function (err) {
        if (err) console.log(err);
    });
}

var incrementFilmWatching = function (filmId){
    Film.findOneAndUpdate({ _id : filmId }, { $inc: { watched: 1 } }, function (err) { 
        if (err) console.log(err);
    });
}

var incrementProfileLogin = function (user){
    User.findOneAndUpdate({ _id: user }, { $inc: { login: 1 } }, function (err) { 
        if (err) console.log(err);
    });
}

var incrementUserFilmAdding = function (user){
    User.findOneAndUpdate({ _id: user }, { $inc: { filmAdded: 1 } }, function (err) {
        if (err) console.log(err);
    });
}

var countUserRates = function (user, callback){
    Rates.find({ userId : user }, function (err, res) {
        if (err || res == 'undefined' || res == null) return callback(0);
        else return callback(res.length);
    });
}

var findMostActiveByAdding = function (number, callback){
    User.find().sort({ filmAdded : 'descending' }).exec(function (err, result) { 
        if (!err) {
            var users = [];
            for (var i = 0; i < result.length; i++) {
                users.push({
                    facebook_name: result[i].facebook.name,
                    facebook_url: result[i].facebook.email,
                    vk_name: result[i].vk.name,
                    vk_url: result[i].vk.email,
                    vk_photo: result[i].vk.photo,
                    google_name: result[i].google.name,
                    google_url: result[i].google.email,
                    google_photo: result[i].google.photo,
                    local_name: result[i].local.email,
                    id: result[i]._id,
                    telegram: result[i].telegram,
                    isAdmin: result[i].admin,
                    filmAdded: result[i].filmAdded
                });
            }
            return callback(users.slice(0, number));
        }
    });
}

var findMostActive = function(number, callback) {
    User.find().sort({ login : 'ascending' }).exec(function (err, result) {
        if (!err) {
            var users = [];
            for (var i = 0; i < result.length; i++) {
                users.push({
                    facebook_name: result[i].facebook.name,
                    facebook_url: result[i].facebook.email,
                    vk_name: result[i].vk.name,
                    vk_url: result[i].vk.email,
                    vk_photo: result[i].vk.photo,
                    google_name: result[i].google.name,
                    google_url: result[i].google.email,
                    google_photo: result[i].google.photo,
                    local_name: result[i].local.email,
                    id: result[i]._id,
                    telegram: result[i].telegram,
                    isAdmin: result[i].admin,
                    filmAdded: result[i].filmAdded
                });
            }
            return callback(users.slice(0, number));
        }
    });
}

var ratesStat = function (callback){
    var rates = {};
    RateMap.find({}, function (err, res) {
        for (curRate of res){
            rates[curRate.rate] = curRate.count;
            }
        return callback(rates);
    });
}

var getNumberOfFilms = function (callback) {
    Film.count({}, function (err, res) {
        return callback(res+1); 
    });
}

module.exports.incrementRatesStat = incrementRatesStat;
module.exports.incrementUserFilmAdding = incrementUserFilmAdding;
module.exports.incrementProfileLogin = incrementProfileLogin;
module.exports.findMostActiveByAdding = findMostActiveByAdding;
module.exports.findMostActive = findMostActive;
module.exports.ratesStat = ratesStat;
module.exports.incrementFilmWatching = incrementFilmWatching;
module.exports.getNumberOfFilms = getNumberOfFilms;