/**
 * Created by Andrian on 12-Jun-16.
 */
function getwords(doc){
    //Split the words by non-alpha characters
    var words=doc.split('[\s\.\\\/ ,:\'\"]');
    dict = {};
    for (var i in words){
        if(length(words[i]) > 2){
            dict[words[i].toLowerCase()] = 1;
        }
    }
    //Return the unique set of words only
    return dict;
}


//Counts of feature/category combinations
var fc={};
//Counts of documents in each category
var cc={};
var getfeatures;

function incf(f,cat){
    fc[f] = fc[f] || {};
    fc[f][cat] = fc[f][cat] || 0;
    fc[f][cat] += 1;
}
function fcount(f,cat){
    if(f in fc && cat in fc[f]){
        return fc[f][cat];
    }
    return 0.0;
}
function incc(cat){
    cc[cat] = cc[cat] || 0;
    cc[cat] += 1;
}
function catcount(cat){
    if (cat in cc){
        return cc[cat];
    }
    return 0;
}

function categories(){
    return Object.keys(cc);
}
function totalcount(){
    var sum = 0;
    for (var i of Object.values(cc)){
        sum += i;
    }
    return sum;
}

function train(self,item,cat) {
    features = getfeatures(item);
    for (var f in features) {
        incf(f, cat)
    }
    incc(cat);
}

function fprob(f,cat) {
    if (catcount(cat) == 0) return 0;
// The total number of times this feature appeared in this
// category divided by the total number of items in this category
    return fcount(f, cat) / catcount(cat);
}
function weightedprob(f,cat,prf,weight=1.0,ap=0.5) {
//Calculate current probability
    var basicprob = prf(f, cat)

//Count the number of times this feature has appeared in all categories
//     var totals = sum([self.fcount(f, c) for c in self.categories()])

//Calculate the weighted average
    return ((weight * ap) + (totals * basicprob)) / (weight + totals);
}



//class naivebayes(classifier):
thresholds={};

function docprob(item,cat) {
    var features = getfeatures(item);
//Multiply the probabilities of all the features together
    var p = 1;
    for (f of features) {
        p *= weightedprob(f, cat, fprob)
    }
    return p
}

function prob(item,cat) {
    var catprob = catcount(cat) / totalcount();
    var docprob = docprob(item, cat);
    return docprob * catprob;
}

function setthreshold(cat,t) {
    thresholds[cat] = t;
}

function getthreshold(cat) {
    if (!(cat in thresholds)){ return 1.0;}
    return thresholds[cat];
}

function classify(item, def) {
    var probs = {};
//Find the category with the highest probability
    var max = 0;
    for (cat of categories()) {
        probs[cat] = prob(item, cat)
        if (probs[cat] > max) {
            max = probs[cat];
            var best = cat;
        }
    }
//Make sure the probability exceeds threshold*next best
    for (cat of probs) {
        if (cat == best) continue;
        if (probs[cat] * getthreshold(best) > probs[best])  return def;
    }
    return best;
}


function sampletrain(cl)
{
    cl.train('Nobody owns the water.', 'good')
    cl.train('the quick rabbit jumps fences', 'good')
    cl.train('buy pharmaceuticals now', 'bad')
    cl.train('make quick money at the online casino', 'bad')
    cl.train('the quick brown fox jumps', 'good')
}
