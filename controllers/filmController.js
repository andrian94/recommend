﻿var imdb = require('node-movie').getByID;

var Film = require('../app/models/film.js');
var Rate = require('../app/models/rate.js');
var NNController = require('./NeuralNetworkController');
var RecommContr = require('./recommendController.js');

var Forbiddenfilms = require('../app/models/forbiddenfilms.js');

var Stat = require('./statController.js');

var addFilm = function (uri, callback) {
    var id = getIdFromUrl(uri);
    if (id == null) return callback("Error");
    checkIfForbidden(id, function (result) {
        if (result == "Forbidden") {
            return callback("Film in db already");
        }
        else {
            imdb(id, function (err, data) {
                if (err == null) {
                    Film.find({imdbID: id}, function (err, result) {
                        if (result.length > 0) {
                            return callback("Film in db already");
                        } else {
                            //var genres = data.Genre.split(", ");
                            var newFilm = new Film({
                                title: data.Title,
                                year: parseInt(data.Year),
                                rated: data.Rated,
                                released: data.Released,
                                runtime: data.Runtime,
                                genre: data.Genre,
                                director: data.Director,
                                writer: data.Write,
                                actors: data.Actors,
                                plot: data.Plot,
                                language: data.Language,
                                country: data.Country,
                                awards: data.Awards,
                                poster: data.Poster,
                                imdbRating: parseFloat(data.imdbRating),
                                imdbVotes: data.imdbVotes,
                                imdbID: data.imdbID,
                                type: data.Type,
                            });
                            newFilm.save(function (err) {
                                if (!err) {
                                    return callback("Film has saved");
                                }
                            });

                        }
                    });
                }
            });
        }
    });

};

var checkIfForbidden = function (imdID, callback) {
    Forbiddenfilms.find({filmID: imdID}, function (err, result) {
        if (err || result == 0) {
            return callback("Allowed or does not exist");
        }
        return callback("Forbidden");
    });
}

var deleteFilm = function (id, callback) {
    //var id = getIdFromUrl(uri);
    //var filmident;
    /*Film.remove({ imdbID: id }, function (err, result) {
     if (!err) { return callback("Deleted"); }
     else {
     return callback("Not deleted");
     }
     })   */
    /*Film.find({ imdbID : id }, function (err, result) {
     if (!err) {
     if (result.length > 0) {
     filmident = result[0]._id;
     Rate.remove({ filmID: filmident }, function (err, res) {
     Film.remove({ _id: filmident }, function (erro, resu) {
     RecommContr.usersTransformer();
     return callback("Done");

     });
     });

     } else { return callback("No film found"); }
     }
     else { return callback("Error"); }
     });*/
    Film.find({_id: id}, function (errr, rresult) {
        if (errr || rresult == 0) {
            return callback("Zero result or error");
        }
        else {
            var forbiden = new Forbiddenfilms({filmID: rresult[0].imdbID});
            forbiden.save(function (err) {
                if (err) {
                    return callback("FILM IS NOT FORBIDDEN");
                }
                else {
                    Film.remove({_id: id}, function (err, result) {
                        if (err || result == 0) {
                            return callback("Zero result or error");
                        }
                        Rate.remove({filmID: id}, function (er, reslt) {
                            if (er || reslt == 0) {
                                return callback("Zero rates");
                            }

                            RecommContr.usersTransformer();
                            return callback("Success");
                        });
                    });
                }
            });

        }
    });


};

var getIdFromUrl = function (url) {
    var result = url.match(/tt[\d]{7}/);
    if (result == null)
        return null;
    else
        return result[0];
}

var getFilm = function (userId, filmId, callback) {
    Film.findOne({_id: filmId}, function (err, result) {
        if (err || result == 'undefined' || result == 0) {

            getRate(userId, filmId, function (rateObject) {
                if ('err' in rateObject) {
                    return callback({});
                }
                var film = {
                    _id: result._id,
                    title: result.title,
                    year: result.year,
                    rated: result.rated,
                    released: result.released,
                    runtime: result.runtime,
                    genre: result.genre,
                    director: result.director,
                    writer: result.writer,
                    actors: result.actors,
                    plot: result.plot,
                    language: result.language,
                    country: result.country,
                    awards: result.awards,
                    poster: result.poster,
                    imdbRating: result.imdbRating,
                    imdbVotes: result.imdbVotes,
                    imdbID: result.imdbID,
                    type: result.type,
                    userRate: rateObject.userRate
                };
                return callback(film);
            });
        }
    });
};

var getAll = function (userId, callback) {
    /*Rate.find({}).populate('filmID').exec(function (err, res) {
     console.log(res);
     return callback(res);
     });*/
    Film.find(function (err, result) {
        if (err || result.length == 0) return callback({});
        var ids = [];
        for (var i in result) {
            ids.push(result[i]._id);
        }

        var films = [];
        for (var j = 0; j < result.length; j++) {
            films.push({
                _id: result[j]._id,
                title: result[j].title,
                year: result[j].year,
                rated: result[j].rated,
                released: result[j].released,
                runtime: result[j].runtime,
                genre: result[j].genre,
                director: result[j].director,
                writer: result[j].writer,
                actors: result[j].actors,
                plot: result[j].plot,
                language: result[j].language,
                country: result[j].country,
                awards: result[j].awards,
                poster: result[j].poster,
                imdbRating: result[j].imdbRating,
                imdbVotes: result[j].imdbVotes,
                imdbID: result[j].imdbID,
                type: result[j].type,
                watched: result[j].watched
            });
        }
        Rate.find({filmID: {$in: ids}, userID: userId}, function (err, res) {
            if (err || res == 'undefined') {
                return callback(films);
            }
            for (var film of films) {
                for (var rate of res) {
                    var f = film._id;
                    var r = rate.filmID;
                    if (f.id == r.id)
                        film.userRate = rate.userRate;
                }
            }
            return callback(films);
        });
    });
}

/*var rateFIlm = function (user, film, newrate, callback) {
 newrate = parseInt(newrate);
 Rate.findOneAndUpdate({ userID: user, filmID: film }, { userRate : newrate }, function (err, result) {
 if (err || result === null) {
 var newRate = new Rate({
 userID: user,
 filmID: film,
 userRate: newrate
 });
 newRate.save(function (err) {
 if (!err) {
 Stat.incrementRatesStat(newrate);
 return callback("Rate is added");
 }
 });
 }
 });
 };*/

var simplePatternMaker = function (words) {
    var pattern = "";
    for (var i in words) {
        pattern += words[i] + ".*";
    }
    return pattern;
};

var genrePatternMaker = function (genres) {
    var pattern = "";
    for (var i = 0; i < genres.length; i++) {
        pattern += "(.*(" + genres[i] + ")+.*)";
        if (i + 1 != genres.length) {
            pattern += "|";
        }
    }

    return pattern;
}

var getFilmsByCriteria = function (userId, /*genres,*/ minYear, maxYear, minImdb, maxImdb, ftype, callback) {
    //var pattern = genrePatternMaker(genres);
    var findObject = {};
    if (ftype !== 'undefined') {     //{ /*genre: { $regex : pattern, $options : 'i' },*/ type: type }
        findObject.type = ftype;
    }
    Film.find(findObject).where('year').gt(minYear).lt(maxYear).where('imdbRating').gt(minImdb).lt(maxImdb).exec(function (err, result) {
        if (err || result.length == 0) return callback({});
        var ids = [];
        for (var i in result) {
            ids.push(result[i]._id);
        }

        var films = [];
        for (var j = 0; j < result.length; j++) {
            films.push({
                _id: result[j]._id,
                title: result[j].title,
                year: result[j].year,
                rated: result[j].rated,
                released: result[j].released,
                runtime: result[j].runtime,
                genre: result[j].genre,
                director: result[j].director,
                writer: result[j].writer,
                actors: result[j].actors,
                plot: result[j].plot,
                language: result[j].language,
                country: result[j].country,
                awards: result[j].awards,
                poster: result[j].poster,
                imdbRating: result[j].imdbRating,
                imdbVotes: result[j].imdbVotes,
                imdbID: result[j].imdbID,
                type: result[j].type
            });
        }
        Rate.find({filmID: {$in: ids}, userID: userId}, function (err, res) {
            if (err || res == 'undefined') {
                return callback(films);
            }
            for (var film of films) {
                for (var rate of res) {
                    var f = film._id;
                    var r = rate.filmID;
                    if (f.id == r.id) film.userRate = rate.userRate;
                }
            }
            return callback(films);
        });
    });
}

var findFilm = function (userId, name, callback) {
    if (name == undefined || name == '') return callback({err_type: "nothing sent"});
    var words = name.split(/\s+\W*/); //split by whitespaces and else shit
    var pattern = simplePatternMaker(words);

    Film.find({title: {$regex: pattern, $options: 'i'}}, function (err, result) {
        if (err == null && result.length > 0) {
            var films = [];
            for (var j = 0; j < result.length; j++) {
                films.push({
                    _id: result[j]._id,
                    title: result[j].title,
                    year: result[j].year,
                    rated: result[j].rated,
                    released: result[j].released,
                    runtime: result[j].runtime,
                    genre: result[j].genre,
                    director: result[j].director,
                    writer: result[j].writer,
                    actors: result[j].actors,
                    plot: result[j].plot,
                    language: result[j].language,
                    country: result[j].country,
                    awards: result[j].awards,
                    poster: result[j].poster,
                    imdbRating: result[j].imdbRating,
                    imdbVotes: result[j].imdbVotes,
                    imdbID: result[j].imdbID,
                    type: result[j].type
                });
            }
            ;

            var ids = [];
            for (var i in result) {
                ids.push(result[i]._id);
            }
            Rate.find({filmID: {$in: ids}, userID: userId}, function (err, res) {
                if (err || res == 'undefined') {
                    return callback(films);
                }
                for (var film of films) {
                    for (var rate of res) {
                        var f = film._id;
                        var r = rate.filmID;
                        if (f.id == r.id)
                            film.userRate = rate.userRate;
                    }
                }
                return callback(films);
            });
        } else return callback({err_type: "incorrect name"});

    });
}

var rateFIlm = function (user, film, newrate, callback) {
    newrate = parseInt(newrate);
    if (film == 'undefined') {
        console.log("Bad request");
    }
    Rate.update({userID: user, filmID: film}, {userID: user, filmID: film, userRate: newrate},
        {upsert: true}, function (err) {
            if (!err) {
                Stat.incrementRatesStat(newrate);
                NNController.propagateUserRate(user);
                RecommContr.usersTransformer(()=> {
                    console.log('transformed')
                });

                return callback({message: "Rate is added"});
            }
        });
}

var getRate = function (user, film, callback) {
    Rate.findOne({userID: user, filmID: film}, function (err, res) {
        if (err) {
            return callback({err: err});
        } else {
            return callback(res);
        }
    });
}

function getArrayOfAllGenres() {
    return ['Action', 'Adventure', 'Comedy', 'Crime', 'Drama', 'Fantasy',
        'History', 'Horror', 'Mystery', 'Romance', 'Sci-Fi', 'Sport', 'Thriller', 'War', 'Western'];
}

module.exports.addFilm = addFilm;
module.exports.getAll = getAll;
module.exports.findFilm = findFilm;
module.exports.rateFilm = rateFIlm;
module.exports.getRate = getRate;
module.exports.getFilm = getFilm;
module.exports.getIdFromUrl = getIdFromUrl;
module.exports.getFilmsByCriteria = getFilmsByCriteria;
module.exports.deleteFilm = deleteFilm;
module.exports.checkIfForbidden = checkIfForbidden;
module.exports.getArrayOfAllGenres = getArrayOfAllGenres;