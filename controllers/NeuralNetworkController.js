/**
 * Created by Andrian on 25-Apr-16.
 */
"use strict";
var UserNeuralNetwork = require('../app/models/userNeuralNetwork');
var Film = require('../app/models/film');
var Rate = require('../app/models/rate');

var synaptic = require('synaptic');
var filmController = require('./filmController');
var nnConfig = require('../config/neuraNetwork');

var allGenres = ['Action', 'Adventure', 'Biography', 'Comedy', 'Crime', 'Drama', 'Family',
    'Fantasy', 'History', 'Horror', 'Mystery', 'Romance', 'Sci-Fi',
    'Sport', 'Thriller', 'War', 'Western'];//filmController.getArrayOfAllGenres();

function formInputForNN(film) {
    var nnInput = new Array(allGenres.length + 3).fill(0);

    var genres = film.genre.split(', ');
    genres.forEach((genre) => {
        let index = allGenres.indexOf(genre);
        if (index >= 0) {
            nnInput[index] = 1;
        }
    });
    var year = (film.year - 1900) / 200;
    var rate = film.imdbRating / 10;
    var type = film.type == 'movie' ? 1 : 0;

    nnInput[nnInput.length - 2] = year;
    nnInput[nnInput.length - 1] = rate;
    nnInput[nnInput.length] = type;

    return nnInput;
}

var propagateUserRate = function (user) {
    var nn;
    UserNeuralNetwork.findOne({userID: user}, (err, networkRes) => {
        if (err || networkRes == 'undefined') {
            return;
        } else if (networkRes == null || networkRes.network == null) {  //create nn if not exist in db for this user
            nn = new synaptic.Architect.Perceptron(nnConfig.inputNeurons, nnConfig.firstHidden,
                nnConfig.secondHidden, /*nnConfig.thirdHidden, nnConfig.fourthHidden,*/ nnConfig.outputNeurons);
        } else {
            nn = synaptic.Network.fromJSON(networkRes.network);
        }
        Rate.find({userID: user}, (err, res) => {
            var films = res.map((film) => {
                return film.filmID
            });
            Film.find({_id: {$in: films}}, (err, films) => {
                if (err || films == undefined || films == null) {
                    return;
                }
                var filmsWithRates = films.map((film)=> {
                    var f = film.toObject();
                    f['userRate'] = res.find((obj)=> {
                        return obj.filmID.toString() == film._id.toString();
                    }).userRate;
                    return f;
                });
                for (var i = 0; i < nnConfig.propagateRounds; i++) {
                    for (var j = 0; j < filmsWithRates.length; j++) {
                        let input = formInputForNN(filmsWithRates[j]);
                        nn.activate(input);
                        var ur = [filmsWithRates[j].userRate / 10];
                        nn.propagate(nnConfig.learningRate, ur);
                    }
                }
                UserNeuralNetwork.update({userID: user}, {
                    userID: user,
                    network: nn.toJSON()
                }, {upsert: true}, (err)=> {
                    if (err) console.log(err);
                });
            });
        });
    });
}

var getNeuralNetworkRecommend = function (user, callback) {
    var scores = [];
    UserNeuralNetwork.findOne({userID: user}, (err, networkRes) => {
        if (err || networkRes == 'undefined' || networkRes == null || networkRes.network == null) {
            return callback([]);
        }
        var nn = new synaptic.Network.fromJSON(networkRes.network);

        Rate.distinct('filmID', (err, films)=> {
            /*var films = res.map((r) => {
             return r.filmID;
             });*/
            Film.find({_id: {$nin: films}}).limit(100).exec((err, unratedFilms) => {
                if (err || unratedFilms == undefined || unratedFilms == null) {
                    return callback([]);
                }
                for (var i = 0; i < unratedFilms.length; i++) {
                    var nnInput = formInputForNN(unratedFilms[i]);
                    var output = nn.activate(nnInput)[0] * 10;
                    scores.push({'name': unratedFilms[i]._id, 'score': output});
                }
                scores.sort(function (a, b) {
                    if (a.score > b.score) return 1;
                    if (a.score < b.score) return -1;
                    else return 0;
                });
                return callback(scores.reverse()/*.filter((score)=> {
                    return score.score >= 0;
                })*/);
            });
        });
    });
}

module.exports.getNeuralNetworkRecommend = getNeuralNetworkRecommend;
module.exports.formInputForNN = formInputForNN;
module.exports.propagateUserRate = propagateUserRate;
/*var input = genres.length + 3; //year, imdb, type [series, film]
 var pool = 50;
 var output = 1;
 var connections = 75;
 var gates = 20;

 var nn = new synaptic.Architect./!*Liquid(input, pool, output, connections, gates)*!/Perceptron(input, 10, 5, 1);

 var trainingSet = [
 {
 input: [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0/!*genres*!/, .109, .8, 1],        //year = (current year - 1990) / 1000
 output: [.9]
 },
 {
 input: [0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0/!*genres*!/, .100, .75, 1],
 output: [.7]
 },
 {
 input: [0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0/!*genres*!/, .112, 0.95, 0],
 output: [1]
 },
 {
 input: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1/!*genres*!/, .095, 0.6, 1],
 output: [.5]
 }
 ];
 nn.trainer.train(trainingSet, {
 rate: .15,
 iterations: 5000000,
 error: .05,
 shuffle: true,
 log: 500000,
 cost: synaptic.Trainer.cost.CROSS_ENTROPY
 });
 console.log(nn.activate([1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0/!*genres*!/, .109, .8, 1]) + "\t9");
 console.log(nn.activate([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1/!*genres*!/, .095, .6, 1]) + "\t5");
 console.log(nn.activate([0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0/!*genres*!/, .100, .75, 1]) + "\t7");
 console.log(nn.activate([0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0/!*genres*!/, .112, .95, 0]) + "\t1");
 console.log(nn.activate([0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0/!*genres*!/, .090, .83, 1]) + "\t8");
 for (var i = 0; i < 100; i++) {
 /!*console.log*!/
 (nn.activate([0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0/!*genres*!/, .090, .81, 1]) + "\t8");
 nn.propagate(0.15, [.8]);
 /!*console.log*!/
 (nn.activate([1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0/!*genres*!/, .109, .8, 1]) + "\t9");
 nn.propagate(0.15, [.9]);
 /!*console.log*!/
 (nn.activate([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1/!*genres*!/, .095, .6, 1]) + "\t5");
 nn.propagate(0.15, [.5]);
 /!*console.log*!/
 (nn.activate([0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0/!*genres*!/, .100, .75, 1]) + "\t7");
 nn.propagate(0.15, [.7]);
 /!*console.log*!/
 (nn.activate([0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0/!*genres*!/, .112, .95, 0]) + "\t1");
 nn.propagate(0.15, [1]);
 }
 console.log(nn.activate([1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0/!*genres*!/, .109, .8, 1]) + "\t9");
 console.log(nn.activate([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1/!*genres*!/, .095, .6, 1]) + "\t5");
 console.log(nn.activate([0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0/!*genres*!/, .100, .75, 1]) + "\t7");
 console.log(nn.activate([0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0/!*genres*!/, .112, .95, 0]) + "\t1");
 console.log(nn.activate([0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0/!*genres*!/, .090, .81, 1]) + "\t8");*/
