﻿var User = require('../app/models/user');

var getUser = function (userId, callback){
    User.findOne({ _id : userId }, function (err, result) {
        if (!err) {
            var UserInfo = {
                facebook: {
                    name: result.facebook.name,
                    url: result.facebook.email
                },
                vk : {
                    name: result.vk.name,
                    url: result.vk.email,
                    photo: result.vk.photo
                },
                google: {
                    name: result.google.name,
                    url: result.google.email,
                    photo: result.google.photo
                },
                local: {
                    name: result.local.email
                },
                id: result._id,
                telegram: result.telegram,
                isAdmin: result.admin
            };
            return callback(UserInfo);
        }
    });
}

var isAdmin = function (userId, callback){
    User.findOne({ _id : userId }, function (err, result) {
        if (err || result == 'undefined') return callback(false);

        return callback(result.admin);
    });
}

var getAllUsers = function (callback){
    User.find({}, function (err, result) {
        if (!err) {
            var users = [];
            for (var i = 0; i < result.length; i++) {
                users.push({
                    facebook_name: result[i].facebook.name,
                    facebook_url: result[i].facebook.email,
                    vk_name: result[i].vk.name,
                    vk_url: result[i].vk.email,
                    vk_photo: result[i].vk.photo,
                    google_name: result[i].google.name,
                    google_url: result[i].google.email,
                    google_photo: result[i].google.photo,
                    local_name: result[i].local.email,
                    id: result[i]._id,
                    telegram: result[i].telegram,
                    isAdmin: result[i].admin
                });
            }
            return callback(users);
        }
    });
}

module.exports.getUser = getUser;
module.exports.isAdmin = isAdmin;
module.exports.getAllUsers = getAllUsers;