/**
 * Created by Andrian on 27-Apr-16.
 */
module.exports = {
    inputNeurons: 20,
    firstHidden: 11,
    secondHidden: 5,
    thirdHidden: 4,
    fourthHidden: 3,
    outputNeurons: 1,
    propagateRounds: 3500,
    learningRate: .033
}