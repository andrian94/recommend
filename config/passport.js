﻿// config/passport.js

// load all the things we need
var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var VkontakteStrategy = require('passport-vkontakte').Strategy;

// load up the user model
var User = require('../app/models/user');

// load the auth variables
var configAuth = require('./auth');

// expose this function to our app using module.exports
module.exports = function (passport) {
    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session
    // used to serialize the user for the session
    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });
    
    // used to deserialize the user
    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            done(err, user);
        });
        /*User.findOne({ "id": id }, function (err, user) {
            done(err, user);
        });*/
    });
    
    
    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    passport.use('local-signup', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    },
    function (req, email, password, done) {
        if (email)
            email = email.toLowerCase(); // Use lower-case e-mails to avoid case-sensitive e-mail matching
        
        // asynchronous
        process.nextTick(function () {
            // if the user is not already logged in:
            if (!req.user) {
                User.findOne({ 'local.email' : email }, function (err, user) {
                    // if there are any errors, return the error
                    if (err)
                        return done(err);
                    
                    // check to see if theres already a user with that email
                    if (user) {
                        return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
                    } else {
                        
                        // create the user
                        var newUser = new User();
                        
                        newUser.local.email = email;
                        newUser.local.password = newUser.generateHash(password);
                        
                        newUser.save(function (err) {
                            if (err)
                                return done(err);
                            
                            return done(null, newUser);
                        });
                    }

                });
            // if the user is logged in but has no local account...
            } else if (!req.user.local.email) {
                // ...presumably they're trying to connect a local account
                // BUT let's check if the email used to connect a local account is being used by another user
                User.findOne({ 'local.email' : email }, function (err, user) {
                    if (err)
                        return done(err);
                    
                    if (user) {
                        return done(null, false, req.flash('loginMessage', 'That email is already taken.'));
                        // Using 'loginMessage instead of signupMessage because it's used by /connect/local'
                    } else {
                        var user = req.user;
                        user.local.email = email;
                        user.local.password = user.generateHash(password);
                        user.save(function (err) {
                            if (err)
                                return done(err);
                            
                            return done(null, user);
                        });
                    }
                });
            } else {
                // user is logged in and already has a local account. Ignore signup. (You should log out before trying to create a new account, user!)
                return done(null, req.user);
            }

        });

    }));

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'
    
    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        session: true,
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function (req, email, password, done) { // callback with email and password from our form
        
        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists
        User.findOne({ 'local.email' : email }, function (err, user) {
            // if there are any errors, return the error before anything else
            if (err)
                return done(err);
            
            // if no user is found, return the message
            if (!user)
                return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash
            
            // if the user is found but the password is wrong
            if (!user.validPassword(password))
                return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.')); // create the loginMessage and save it to session as flashdata
            
            // all is well, return successful user
            return done(null, user);
        });

    }));

    // =============================================================================================================================
    // FACEBOOK ================================================================
    // =========================================================================
    passport.use(new FacebookStrategy({
        // pull in app id and secret from auth.js file
        clientID        : configAuth.facebookAuth.clientID,
        clientSecret    : configAuth.facebookAuth.clientSecret,
        callbackURL     : configAuth.facebookAuth.callbackURL,
        enableProof: true,
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    },

    // facebook will send back the token and profile
    function (req ,token, refreshToken, profile, done) {
        console.log(profile);
        //done(null, profile);
        // asynchronous
        process.nextTick(function () {
            
            // check if the user is already logged in
            if (!req.user) {
                
                // find the user in the database based on their facebook id
                User.findOne({ 'facebook.id' : profile.id }, function (err, user) {
                    
                    // if there is an error, stop everything and return that
                    // ie an error connecting to the database
                    if (err)
                        return done(err);
                    
                    // if the user is found, then log them in
                    if (user) {
                        // if there is a user id already but no token (user was linked at one point and then removed)
                        // just add our token and profile information
                        if (!user.facebook.token) {
                            user.facebook.token = token;
                            user.facebook.name = profile.name.displayName;
                            user.facebook.email = profile.profileUrl;
                            
                            user.save(function (err) {
                                if (err)
                                    throw err;
                                return done(null, user);
                            });
                        }
                        return done(null, user);
                    } else {
                        // if there is no user found with that facebook id, create them
                        var newUser = new User();
                        
                        // set all of the facebook information in our user model
                        newUser.facebook.id = profile.id; // set the users facebook id                   
                        newUser.facebook.token = token; // we will save the token that facebook provides to the user                    
                        newUser.facebook.name = profile.displayName;//name.givenName + ' ' + profile.name.familyName; // look at the passport user profile to see how names are returned
                        
                        newUser.facebook.email = profile.profileUrl;
                        
                        // save our user to the database
                        newUser.save(function (err) {
                            if (err)
                                throw err;
                            
                            // if successful, return the new user
                            return done(null, newUser);
                        });
                    }

                });

            } else {
                // user already exists and is logged in, we have to link accounts
                var user = req.user; // pull the user out of the session
                
                // update the current users facebook credentials
                user.facebook.id = profile.id;
                user.facebook.token = token;
                user.facebook.name = profile.displayName;
                user.facebook.email = profile.profileUrl;
                
                // save the user
                user.save(function (err) {
                    if (err)
                        throw err;
                    return done(null, user);
                });
            }

        });

    }));

    // =====================================================================================================
    // GOOGLE ==================================================================
    // =========================================================================
    passport.use(new GoogleStrategy({
        clientID        : configAuth.googleAuth.clientID,
        clientSecret    : configAuth.googleAuth.clientSecret,
        callbackURL     : configAuth.googleAuth.callbackURL,
        enableProof: true,
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    },
     function (req , token, refreshToken, profile, done) {
        
        // asynchronous
        process.nextTick(function () {
            
            // check if the user is already logged in
            if (!req.user) {
                
                // find the user in the database based on their facebook id
                User.findOne({ 'google.id' : profile.id }, function (err, user) {
                    
                    // if there is an error, stop everything and return that
                    // ie an error connecting to the database
                    if (err)
                        return done(err);
                    
                    // if the user is found, then log them in
                    if (user) {
                        // if there is a user id already but no token (user was linked at one point and then removed)
                        if (!user.google.token) {
                            user.google.token = token;
                            user.google.name = profile.displayName;
                            user.google.email = profile.emails[0].value; // pull the first email
                            user.google.photo = profile.photos[0].value;
                            user.save(function (err) {
                                if (err)
                                    throw err;
                                return done(null, user);
                            });
                           
                        }
                        return done(null, user);
                    } else {
                        // if there is no user found with that facebook id, create them
                        var newUser = new User();
                        
                        // set all of the facebook information in our user model
                        newUser.google.id = profile.id;
                        newUser.google.token = token;
                        newUser.google.name = profile.displayName;//name.givenName + ' ' + profile.name.familyName;
                        newUser.google.email = profile.emails[0].value;
                        newUser.google.photo = profile.photos[0].value;
                        // save our user to the database
                        newUser.save(function (err) {
                            if (err)
                                throw err;
                            
                            // if successful, return the new user
                            return done(null, newUser);
                        });
                    }

                });

            } else {
                // user already exists and is logged in, we have to link accounts
                var user = req.user; // pull the user out of the session
                
                // update the current users facebook credentials
                user.google.id = profile.id;
                user.google.token = token;
                user.google.name = profile.displayName;//name.givenName + ' ' + profile.name.familyName;
                user.google.email = profile.emails[0].value;
                user.google.photo = profile.photos[0].value;
                // save the user
                user.save(function (err) {
                    if (err)
                        throw err;
                    return done(null, user);
                });
            }

        });

    }));
    
    // =====================================================================================================
    // VK ==================================================================
    // =========================================================================
    passport.use(new VkontakteStrategy({
        clientID        : configAuth.vkAuth.clientID,
        clientSecret    : configAuth.vkAuth.clientSecret,
        callbackURL     : configAuth.vkAuth.callbackURL,
        //enableProof: false,
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    },
    function (req , token, refreshToken, profile, done) {
        
        // asynchronous
        process.nextTick(function () {
            
            // check if the user is already logged in
            if (!req.user) {
                
                // find the user in the database based on their facebook id
                User.findOne({ 'vk.id' : profile.id }, function (err, user) {
                    
                    // if there is an error, stop everything and return that
                    // ie an error connecting to the database
                    if (err)
                        return done(err);
                    
                    // if the user is found, then log them in
                    if (user) {
                        // if there is a user id already but no token(user was linked at one point and then removed)
                        // just add our token and profile information
                        if (!user.vk.token) {
                            user.vk.token = token;
                            user.vk.name = profile.name.displayName;
                            user.vk.email = profile.profileUrl;
                            user.vk.photo = profile.photos[0].value;
                            user.save(function (err) {
                                if (err)
                                    throw err;
                                return done(null, user);
                            });

                        }
                        return done(null, user);

                    } else {
                        // if there is no user found with that facebook id, create them
                        var newUser = new User();
                                            
                        newUser.vk.id = profile.id;                 
                        newUser.vk.token = token;                    
                        newUser.vk.name = profile.displayName;                       
                        newUser.vk.email = profile.profileUrl;
                        newUser.vk.photo = profile.photos[0].value;
                        // save our user to the database
                        newUser.save(function (err) {
                            if (err)
                                throw err;
                            
                            // if successful, return the new user
                            return done(null, newUser);
                        });
                    }

                });

            } else {
                // user already exists and is logged in, we have to link accounts
                var user = req.user; // pull the user out of the session
                
                user.vk.id = profile.id;                 
                user.vk.token = token;                    
                user.vk.name = profile.displayName;
                user.vk.email = profile.profileUrl;
                user.vk.photo = profile.photos[0].value;
                // save the user
                user.save(function (err) {
                    if (err)
                        throw err;
                    return done(null, user);
                });
            }

        });
    }));

};
