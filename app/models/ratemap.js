﻿// app/models/ratemap.js
var mongoose = require('mongoose');

var schema = mongoose.Schema({
    rate : Number,
    count : Number
});

module.exports = mongoose.model("RateMap", schema);