/**
 * Created by Andrian on 19-Apr-16.
 */
var mongoose = require('mongoose');

var schema = mongoose.Schema({
    similarUsers : {}
});

module.exports = mongoose.model("SimilarUsers", schema);