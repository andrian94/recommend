/**
 * Created by Andrian on 19-Apr-16.
 */
var mongoose = require('mongoose');

var schema = mongoose.Schema({
    similarItems : {}
});

module.exports = mongoose.model("SimilarItems", schema);