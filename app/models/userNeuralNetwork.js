/**
 * Created by Andrian on 27-Apr-16.
 */
var mongoose = require('mongoose');

var userNNSchema = mongoose.Schema({
    userID: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    network: {type: mongoose.Schema.Types.Mixed}
});

module.exports = mongoose.model('UserNeuralNetwork', userNNSchema);
