﻿// app/models/film.js
var mongoose = require('mongoose');

var filmSchema = mongoose.Schema({
    title: String,
    year: Number,
    rated: String,
    released: String,
    runtime: String,
    genre: String,
    director: String,
    writer:String,
    actors: String,
    plot: String,
    language: String,
    country: String,
    awards: String,
    poster: String,
    imdbRating: Number,
    imdbVotes: String,
    imdbID: String,
    type: String,
    watched: Number
});

module.exports = mongoose.model('Film', filmSchema);

