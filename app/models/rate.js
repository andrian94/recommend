﻿// app/models/rate.js
var mongoose = require('mongoose');

var rateSchema = mongoose.Schema({
    userID: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    filmID: {type: mongoose.Schema.Types.ObjectId, ref: 'Film'},
    userRate: Number
});

module.exports = mongoose.model('Rate', rateSchema);

