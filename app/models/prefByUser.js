﻿// app/models/prefByUser.js
var mongoose = require('mongoose');

var schema = mongoose.Schema({
    users : {},
    similarUsers: {},
    similarItems: {}
});

module.exports = mongoose.model("PrefByUser", schema);