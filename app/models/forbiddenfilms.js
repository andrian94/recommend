﻿// app/models/film.js
var mongoose = require('mongoose');

var forbiddenfilmsSchema = mongoose.Schema({
    filmID: String
});

module.exports = mongoose.model('Forbiddenfilms', forbiddenfilmsSchema);

