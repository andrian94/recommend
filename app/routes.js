﻿module.exports = function (app, passport, film, userController, recommendControl, stat) {    

    rateUrl = '/rates.html';  //redirect to static file sending if login
    //Adding film from imdb
    app.post('/film/add', function (req, res) {
        if (req.body.id != undefined || req.body.id !== '') {
            film.addFilm(req.body.id, function (message) {
                if (message == "Film has saved") {
                    stat.incrementUserFilmAdding(req.user._id);
                }
                res.json(message);
            });
        }
    });
    //deleting film with imdb id
    app.post('/film/delete', function (req, res) {
        if (req.body.id != undefined || req.body.id !== '') {
            film.deleteFilm(req.body.id, function (message) {//returns "Done" if everything is ok
                res.json(message);
            });
        }
    });
    
    app.get('/film/recommendations', isLoggedIn, function (req, res) {
        recommendControl.getRecommendedFilms(req.user._id, function (result) { 
            res.json(result);
        });
    });
    
    app.post('/film/criteria', isLoggedIn, function (req, res) {
        film.getFilmsByCriteria(req.user._id/*, req.body.genres*/, req.body.minYear, req.body.maxYear, req.body.minImdb, req.body.maxImdb, req.body.type, function (films) { 
            res.json(films);
        });
    });
    
    //Get all films
    app.get('/film/all', isLoggedIn, function (req, res) {
        film.getAll(req.user._id, function (result) { 
            res.json(result);
        });
        
    });
    
    //Rate film
    app.post('/film/rate', isLoggedIn, function (req, res) {
        if (req.body.filmId === "undefined" || req.body.filmId === null) res.json("invalid filmId");
        film.rateFilm(req.user._id, req.body.filmId, req.body.rate, function (message) {
            if (message == 'Rate is added') {
                stat.incrementRatesStat(req.body.rate);
            }
            res.json(message);
        });
    });
    
    //Get user rate for given film
    app.post('/film/getRate', isLoggedIn, function (req, res) {
        film.getRate(req.user._id, req.body.filmId, function (result){
            res.json(result);
        })
    });
    
    //get film by id
    app.post('/getFilm', isLoggedIn, function (req, res) {
        film.getFilm(req.body.filmId, function (result) { 
            res.json(result);
        });
    });
    
    //Find film
    app.post('/film/find', isLoggedIn, function (req, res) {
        film.findFilm(req.user._id, req.body.name, function (result) { 
            res.json(result);
        });
    });
    
    //if anybody click on film more info
    app.post('/film/watch', isLoggedIn, function (req, res) { 
        stat.incrementFilmWatching(req.body.film);
    });
    
    //number of films in collection
    app.get('/film/number', isLoggedIn, function (req, res) {
        stat.getNumberOfFilms(function (result) { 
            res.json({ number: result });
        });
    });
    
    app.get('/rates/statistic', isLoggedIn, function (req, res) {
        stat.ratesStat(function (result) { 
            res.json(result);
        });
    });
    
    //recomend users
    app.get('/user/recommendation', function (req , res) {
        recommendControl.getSimilarUsers(req.user._id, function (result) { 
            res.json(result);
        });
    });
    
    //get info about user
    app.get('/user', isLoggedIn, function (req, res) {
        userController.getUser(req.user._id, function (result) { 
            res.json(result)
        });
    });
    
    app.post('/user/mostActive', isLoggedIn, function (req, response) {
        userController.isAdmin(req.user._id, function (res) {
            if (res) {
                stat.findMostActive(req.body.number, function (mostActive) {
                    response.json(mostActive);
                });
            } else {
                response.redirect('/profile.html');
            }
        });
    });
    
    app.post('/user/mostActiveAdding', isLoggedIn, function (req, response) {
        userController.isAdmin(req.user._id, function (res) {
            if (res) {
                stat.findMostActiveByAdding(req.body.number, function (mostActive) {
                    response.json(mostActive);
                });
            } else {
                response.redirect('/profile.html');
            }
        });
    });
    // get all users for admin... anal bunishment is coming
    app.get('/user/allUsers', isLoggedIn, function (req, response) {//added by Oleksiy. Don't kill me, if sthing goes totally wrong:)
        userController.isAdmin(req.user._id, function (res) {
            if (res) {
                userController.getAllUsers(function (result) {
                    response.json(result);
                });
            } else {
                response.redirect('/profile.html');
            }
        });
    });

    // =====================================
    // HOME PAGE (with login links) ========
    // =====================================
    app.get('/', function (req, res) {
        res.render('index.ejs'); // load the index.ejs file
    });
    
    // =============================================================================================
    // LOGOUT ==============================
    // =====================================
    app.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });
    
    // =================================================================================================
    // PROFILE SECTION =====================
    // =====================================
    // we will want this protected so you have to be logged in to visit
    // we will use route middleware to verify this (the isLoggedIn function)
    /*app.get('/profile', isLoggedIn, function (req, res) {
        res.render('profile.ejs',{
            user : req.user // get the user out of session and pass to template
        });
        
        res.send()
    });*/
    
    // ===================================================================================================================
    // AUTHENTICATE (FIRST LOGIN) ==================================================
    // ===================================================================================================================
    
    //local
        // LOGIN ===============================
        // show the login form
        app.get('/login', function (req, res) {
            // render the page and pass in any flash data if it exists
            res.render('login.ejs', { message: req.flash('loginMessage') });
        });
        // process the login form
        app.post('/login', passport.authenticate('local-login', {
            successRedirect : rateUrl,
            failureRedirect : '/login', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
        }));
    
        // SIGNUP ==============================
        // show the signup form
        app.get('/signup', function (req, res) {
            // render the page and pass in any flash data if it exists
            res.render('signup.ejs', { message: req.flash('signupMessage') });
        });
    
        // process the signup form
        app.post('/signup', passport.authenticate('local-signup', {
            successRedirect : rateUrl, 
            failureRedirect : '/signup', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
        }));
    
    // FACEBOOK ============================
        // route for facebook authentication and login
        app.get('/auth/facebook', passport.authenticate('facebook', { scope : ['public_profile',] }));
    
        // handle the callback after facebook has authenticated the user
        app.get('/auth/facebook/callback',
            passport.authenticate('facebook', {
            successRedirect : rateUrl,
            failureRedirect : '/'
        }));
    
    // GOOGLE ==============================
        // =====================================
        // send to google to do the authentication
        // profile gets us their basic information including their name
        // email gets their emails
        app.get('/auth/google', passport.authenticate('google', { scope : ['profile', 'email'] }));
    
        // the callback after google has authenticated the user
        app.get('/auth/google/callback',
                passport.authenticate('google', {
            successRedirect : rateUrl,
            failureRedirect : '/'
        }));
    
    // VK ==================================
        // =====================================
        app.get('/auth/vkontakte', passport.authenticate('vkontakte', { scope : 'email' }, {response_type:'token'}));   
    
        app.get('/auth/vkontakte/callback',
                passport.authenticate('vkontakte', {
            successRedirect : rateUrl,
            failureRedirect: '/'
        }));

    // =============================================================================
    // AUTHORIZE (ALREADY LOGGED IN / CONNECTING OTHER SOCIAL ACCOUNT) =============
    // =============================================================================
    
    // locally --------------------------------
        app.get('/connect/local', function (req, res) {
            res.render('connect-local.ejs', { message: req.flash('loginMessage') });
        });
        app.post('/connect/local', passport.authenticate('local-signup', {
            successRedirect : '/profile.html',
            failureRedirect : '/connect/local', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
        }));
    
    // facebook ------------------------------- 
        // send to facebook to do the authentication
        app.get('/connect/facebook', passport.authorize('facebook', { scope : 'email' }));
    
        // handle the callback after facebook has authorized the user
        app.get('/connect/facebook/callback',
                passport.authorize('facebook', {
            successRedirect : '/profile.html',
            failureRedirect : '/'
        }));

    // google ---------------------------------
        // send to google to do the authentication
        app.get('/connect/google', passport.authorize('google', { scope : ['profile', 'email'] }));
    
        // the callback after google has authorized the user
        app.get('/connect/google/callback',
                passport.authorize('google', {
            successRedirect : '/profile.html',
            failureRedirect : '/'
        }));

    // VK ==================================
        // =====================================
        app.get('/connect/vkontakte', passport.authorize('vkontakte', { scope : 'email' }, { response_type: 'token' }));
    
        app.get('/connect/vkontakte/callback',
                    passport.authenticate('vkontakte', {
            successRedirect : '/profile.html',
            failureRedirect: '/'
        }));
    

    // =============================================================================
    // UNLINK ACCOUNTS =============================================================
    // =============================================================================
    // used to unlink accounts. for social accounts, just remove the token
    // for local account, remove email and password
    // user account will stay active in case they want to reconnect in the future
    
    // local -----------------------------------
    app.get('/unlink/local', function (req, res) {
        var user = req.user;
        user.local = undefined;
        //user.local.password = undefined;
        user.save(function (err) {
            res.redirect('/profile.html');
        });
    });
    
    // facebook -------------------------------
    app.get('/unlink/facebook', function (req, res) {
        var user = req.user;
        user.facebook = undefined;
        user.save(function (err) {
            res.redirect('/profile.html');
        });
    });
    
    // vk --------------------------------
    app.get('/unlink/vkontakte', function (req, res) {
        var user = req.user;
        user.vk = undefined;
        user.save(function (err) {
            res.redirect('/profile.html');
        });
    });
    
    // google ---------------------------------
    app.get('/unlink/google', function (req, res) {
        var user = req.user;
        user.google = undefined;
        user.save(function (err) {
            res.redirect('/profile.html');
        });
    });
};

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {
    
    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();
    // if they aren't redirect them to the home page
    res.redirect('/');
}